# App Terminal

## Overview
The Terminal library by Coarpro LLC can be used to run shell commands direcly from your application to help automate processes, and control other things that can be done via Command Line Interface (CLI). This library provides great features to reduce the amount of logic included in your code and provides enough flexibility to integrate with whatever design strategy you choose to go with when creating your automation program.

## NOTICE
The developer who created this library is raising funds to repair his home that was severely damaged due to a burst pipe resulting in 3ft of sitting water on the lower level. Please donate to help raise $3000 to fund the repair costs by contributing to the GoFundMe at

 https://gofund.me/d27b1b87.

## Table of Contents
- [Requirements](#requirements)
- [Obtaining / Consuming](#obtaining--consuming)
- [Features and Functionality](#features-and-functionality)
- [Creating A Terminal Instance](#creating-a-terminal-instance)
    - [For Linux -- or any OS based off the Unix platform](#for-linux----or-any-OS-based-off-the-Unix-platform)
    - [For Microsoft Windows -- or any other operating system not based off the Unix platform](#For-Microsoft-Windows----or-any-other-operating-system-not-based-off-the-Unix-platform)
- [Executing Commands](#executing-commands)
    - [Execute A Command And Get Stdout](#execute-a-command-and-get-stdout)
    - [Execute A Command Without Getting Stdout](#execute-a-command-without-getting-stdout)
    - [Execute A Command That Includes Quotes](#execute-a-command-that-includes-quotes)
        - [For Windows](#for-windows)
        - [For Linux](#for-linux)
- [Creating A Terminal Instance With Non Default Behavior](#creating-a-terminal-instance-with-non-default-behavior)
    - [Advanced Example](#advanced-example)
- [Multithreading](#multithreading)
- [Terminal Exceptions](#terminal-exceptions)
- [Method Explanations](#method-explanations)
    - [Terminal Interface Method Explanations](#terminal-interface-method-explanations)
    - [Terminal Builder Class Method Explanations](#terminal-builder-class-method-explanations)
- [Issues/Bugs/Enhancement Requests](#issues-bugs-enhancement-requests)

## Features and Functionality
The terminal library provides the following features and functionality:
* Support for any Linux based OS and Microsoft Windows
* Support for running commands that include double quotes `"` or single quotes `'` or both
* Multithreading support
* Returning stdout
* Returning stderr
* Setting timeout limits on commands
* Running commands as different user (Linux based OS's only)
* Running commands with a defined shell instead of the default i.e `/usr/bin/bash` or `/usr/bin/ksh`
or `cmd`

## Requirements
- OpenJDK 17+

## Obtaining / Consuming
This library can be installed from [Maven Central](https://central.sonatype.com/artifact/com.coarpro.libraries/terminal) repositories. Add the following snippet to your `pom.xml` file

```xml
<dependency>
  <groupId>com.coarpro.libraries</groupId>
  <artifactId>terminal</artifactId>
  <!--Specifiy the version youd like to use-->
  <version>${version}</version>
</dependency>
```

**NOTE**: The below examples use Linux but the functionality applies to Microsoft Windows as well (unless otherwise noted).

## Creating A Terminal Instance
All terminal classes use a builder class and use `.build()` method to create a class instance. See relevant section
[Terminal Builder methods](#terminal-class-builder-method-explanations) for explanations of these methods.

Use the relevant terminal class for your operating system.

#### For Linux -- or any OS based off the Unix platform
Use Linux `LinuxTerminalBuilder`
```java
Terminal terminal = new LinuxTerminal.LinuxTerminalBuilder().build()
```

#### For Microsoft Windows -- or any other operating system not based off the Unix platform
Use Generic Terminal `GenericTerminal`
```java
Terminal terminal = new GenericTerminal.GenericTerminalBuilder().build()
```

## Executing Commands
After a terminal instance has been created you can then begin to run commands. You can choose to run the a command immediately,
or you can choose to set the command to execute, and then run it when the time allows.

### Execute A Command And Get Stdout
This sets a command to run and immediately executes it.
```java
String stdout = terminal.runShellCommandAndGetOutput("echo hello");

// hello
System.out.println(stdout);
```

### Execute A Command Without Getting Stdout
You can set a shell command to run and execute it later.

```java
terminal.setShellCommand("echo hello");

// Runs the command.
terminal.runShellCommand();
```

### Execute A Command That Includes Quotes
If part of your command includes double quotes then you will need to follow strict rules to allow proper escaping based on your platform.

#### **For Windows**
- Double Quotes `"`: Include three backslashes for each double quote so that they are escaped.
- Single Quotes `'`: No escaping is needed.

```java
String shellCommand = "Write-Host 'This returned a string with \\\"double quotes\\\"'";

String stdout = terminal.runShellCommandAndGetOutput(shellCommand);

// This returned a string with "double quotes"
System.out.println(stdout);
```

#### **For Linux**

- Double Quotes: `"`: Include one backslash for each double quote so that they are escaped.
- Single Quotes: `'`: No escaping is needed.

**TIL**: As a quick TIL (Today I learned), when executing a shell command through Java on Linux you dont necessarily need to surround part of your command in quotes and can generally remove them. The quotes are more of a Shell syntax, and is an indication to the shell to not interpret the next part of text after a space as a separate command or parameter. In most cases this is fine but if you are still having trouble then use the following example to parse backslashes.

```java
String shellCommand = "echo 'This returned a string with \"double quotes\"'";

String stdout = terminal.runShellCommandAndGetOutput(shellCommand);

// This returned a string with "double quotes"
System.out.println(stdout);

/*
 * Another example to create a directory that contains a space.
 * Again, this isnt common since its more feasible to just
 * create a directory through Java using built in methods as opposed
 * to going through to the shell but its being shown to demonstrate
*/
terminal.setShellCommand("mkdir -p \"/tmp/spaced directory\"");
terminal.runShellCommand();

// 0
System.out.println(terminal.getExitCode());
```

## Creating A Terminal Instance With Non Default Behavior
Depending on how you are designing your application sometimes you may need to change default behavior.

Consider the following scenario:

You are building an application that will automate many different applications and
1. Some commands will be run as another user
2. Some commands will need to have a longer timeout than the default
3. Some error codes will be safe to ignore -- such as if a service is already running or stopped then error code 1 can be ignored
4. One of the commands that will be executed needs to have a specific error code ignored but any other command should not ignore that error code
5. The default shell configured on the OS that will run on is not the one you're familiar with running commands from so you want to use a different shell.


The terminal library helps with all of this. The example below will show how this library handles all these scenarios.

***NOTE***: The following example uses Linux but the functionality is also present in Microsoft Windows (unless otherwise noted)

### Advanced Example
We will assume that we want to update the system packages installed on Linux and that Atlassian Jira is installed on this node and
we want to start it. We will then run one command as `root` and another as the jira OS account.

```java
// Create terminal for OS account root.
Terminal  rootUserTerminal = new LinuxTerminal.LinuxTerminalBuilder()
                                            .withSpecifiedShell("/usr/bin/bash")
                                            .withDefaultMinutesToTimeOut(15)
                                            .withIgnorableExitCodes()
                                            .build();
// Create terminal for Jira OS account
Terminal jiraUserTerminal = new LinuxTerminal.LinuxTerminalBuilder()
                                            .withSpecifiedShell("/usr/bin/bash")
                                            .withUserAccountToExecuteCommands("jira")
                                            .withDefaultMinutesToTimeOut(20)
                                            .withIgnorableExitCodes(1)
                                            .build();

/*
The library will run these commands using the shell specified
assuming that shell is installed.
*/

// Update the System packages
rootUserTerminal.setShellCommand("yum update -y");
rootUserTerminal.runShellCommand();

/*
 * If desired, you can get its standard output
 */
String rootCommandOutput = rootUserTerminal.getShellCommandOutput();

```
Now lets assume that after updating system packages that you want to start Jira and you dont really need an exception raised if Jira is already running or not. For example, if you run the Linux utility `lsof` to check if a directory has running files from it and theres no open files from that directory then it will throw exit code 1. Therefore we need to ignore this exit code. But any other command issued AFTER the `lsof` command should raise an exception if its error code is >=1 since we dont want to permanently ignore that exit code.

```java

/*
(1.) Pass either a comma separated integer args
or an integer array of exit codes that should be
ignored ONE TIME for an upcoming command
*/
jiraUserTerminal.setOneTimeIgnorableExitCode(1);

/*
(2.)  Set the shell command to be executed.
We want to check if there are running files from the Jira
directory.
*/
jiraUserTerminal.setShellCommand("lsof /opt/atlassian/jira/");

/*
 (3) check for open files.
 It will not error if exit code 1 is returned
 since we have instructed the library to halt
 raising an exception this one time.
*/
jiraUserTerminal.runShellCommand();

/*
 (4.) Start Jira
 If Jira fails to start and errors with exit code 1 then an exception
 will be raised. This is because we ignored exit code 1 just one time
 for the previous command.
*/
jiraUserTerminal.setShellCommand("/opt/atlassian/jira/current/bin/start-jira.sh");
jiraUserTerminal.runShellCommand();


String jiraCommandOutput = jiraUserTerminal.getShellCommandOutput();
```

## Multithreading
The library supports Java Multithreading options so that your application can perform more tasks at once. In the example above the library waited for the system packages to be updated before starting Atlassian Confluence Jira. This is known as blocking. Your automation program can perform multiple operations using this library without blocking. Using the example above you can do the following

```java
Thread rootTerminalThread = new Thread(rootUserTerminal);

Thread jiraTerminalThread = new Thread(jiraUserTerminal);

// Now run both commands
rootTerminalThread.start();
jiraTerminalThread.start();
```

Your application will now execute both commands at once instead of waiting for one command to complete before moving onto the next command. This now allows your application to perform other tasks while those two tasks are taking place such as starting something in a cloud based service. You can check back later on if the command has failed or timed out and get its stdout if necessary.

```java
/*
Check if the thread is  no
longer alive.
*/
if (!rootTerminalThread.isAlive()) {
    Boolean hasRootUserShellCommandFailed = rootTerminalUser.getHasShellCommandFailed();
}

if (!jiraTerminalThread.isAlive()) {
    Boolean hasJiraUserShellCommandFailed = jiraTerminalUser.getHasShellCommandFailed();
}
```

**NOTE**: Exceptions are not raised when used with multithreading. The Terminal library will catch and handle the exceptions accordingly. You can obtain the stdout, stderr, exit code if needed by calling methods `.getStdout()`, `.getStderr()`, `.getExitcode()`, `getHasShellCommandTimedOut()`, `getHasShellCommandFailed()`, `getShellOutput`

## Terminal Exceptions
When a given shell command does not exit with exit code 0 or if a shell command times out then a `TerminalException` is thrown. Before a termianl exception is thrown the program will attempt to get its stdout, stderr, exit code and then throw the exception. This allows you to catch the exception and inspect the
stdout and perform additional logic if desired. An example is provided below:

```java
try {
    rootUserTerminal.setShellCommand("yum update -f");
    rootUserTerminal.runShellCommand();

    catch(TerminalException e) {
        String error = e.getMessage();
        String stdout = rootUserTerminal.getStdout();
        String stderr = rootUserTerminal.getStderr();
        Integer exitCode = rootUserTerminal.getExitCode();
        Boolean hasTimedOut = rootUserTerminal.getHasShellCommandTimedOut();
        Boolean hasFailed = rootUserTerminal.getHasShellCommandFailed();

        // Add additional logic as needed.
    }
}
```
## Method Explanations
The following sections explain the most commonly used methods you may use with your automation program.
The descriptions are generic. Not all methods are explained here. Review the code documentation (javadoc) for more in-depth explanations.

### Terminal Interface Method Explanations
Below is a list of the most common methods you will use with this library.
For more indepth descriptions, and for any other methods that are not shown here can be reviewed by reviewing the codebase

|Method Name   |   Explanation||   |   |
|---|---|:---:|:---:|:---:|
|.runCommand               |It is used to run a command. It throws an exception if a command fails unless it hits an ignorable exit code.   |
|.runCommandAndGetOutput() |It is used to run a command and return its output. It throws an exception if a command fails unless it hits an ignorable exit code                                                                                                                                        |
|.run()                    |It is used to run a command. It DOES NOT throw an exception if a command fails. Used with multithreading.       |
|.getShellCommandOutput()  |It returns the shell command output. If it errors then it will show both is stdout and stderr                   |
|.getStdout()              | It returns the stdout of the shell command run                                                                 |
|.getStderr()              | It returns the stderr of the shell command run                                                                 |
|.getHasShellCommandTimedOut()           | It returns a boolean value if a shell command timed out.                                         |
|.getHasShellCommandFailed()             | It returns a boolean value if the shell command failed.                                          |
|.setShellCommand(String shellCommand)   |It sets the shell command to be run                                                               |
|.setOneTimeIgnorableExitCodes           |It sets exit codes that should be ignored for an upcoming command to be issued.                   |
|.setOneTimeMinutesToTimeOut             | It sets the timeout limit to timeout for an upcoming command.                                    |

### Terminal Builder Class Method Explanations
Below is a brief explanation of the builder methods used to build an instance of Terminal. For more indepth descriptions, review the codebase.

|Method Name   |   Explanation||   |   |
|---|---|:---:|:---:|:---:|
|.withDefaultMinutesToTimeOut      |It sets the class behavior to force kill any command run through the class instance if it runs longer than the allocated time. Default is 15 minutes if not specified.                                                          |
|.withUserAccountToExecuteCommands | It sets the class behavior to run any command passed through in the class instance as the OS account specified. Default is the current OS account your application is launched under. This functionality is not currently available in non Linux based OS's.|
|.withLogger                       |It sets the class behavior to log output to the Logger provided by Coarpro LLC. Default is none which causes nothing to be logged.      |
|.withIgnorableExitCodes           |It sets the class behavior to prevent raising exceptions for any command run through the class instance if any command hits any of the given error codes. Default is empty.                            |
|.withSpecifiedShell               | It sets the class behavior to run all commands through the class instance using the specified shell. Default is the default shell configured on the OS which on Windows this is usually `cmd` and on Linux is usually either `/usr/bin/sh` or `/usr/bin/bash`                                                                               |
|.build()                          | It builds an instance of the specified Terminal and returns the Terminal interface                                                                                                    |


## Issues, Bugs, Enhancement Requests
This library was developed by Christopher. If you have an enhancement request then he'd gladly like to hear your proposal, and may consider it. Reach out on here. This procedure is also valid for bugs you may encounter.