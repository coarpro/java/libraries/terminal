# CHANGELOG
This file documents changes over the lifetime of the library.

## 2.1.0
- Updated switch user test to use `sudo` with `su` to prevent possible passwor storage in the linux command history.
- Renamed local variable to `shellCommandtoRun` to avoid ambiguity with the class variable.
- Bug fix: Updated method `build` in `LinuxTerminal` to remove prepending `su`.
- Enhancement: Applied better logic to building a shell command when running a command should be ran as another user (`LinuxTerminal`)
- Bug fix: Added output for the reason an elevated shell command fails when testing an elevated command when building an instance of `LinuxTerminal`
- Removed MagicNumber chekstyle

## 2.0.2
- Fix bug that doesnt parse commands with spaces properly.

## 2.0.1
- Updated readme documentation on how to obtain and install and specified minimum Java version

## 2.0.0
 - Replace class and method `Runtime.getRuntime.exec()` with `ProcessBuilder` as Runtime.exec() is deprecated in Java 18
    - Changed data type of class variable `shellCommand` from `String` to `List<String>` in abstract class `Shell` to allow better stability when passing a shell command.
    - Modified class `GenericTerminal` to be compatbile with design change of `Shell` class
    - Modified `LinuxTerminal` to be compatible with design change of `Shell` class
    - Create new Factory class `TerminalFactory`
    - Created new buildable method `withTerminalFactory` to be able to create an instance of `TerminalFactory` class
    - Added functionality in `Shell` class to automatically create a `TerminalFactory` if an instance of `Shell` was constructed without one.
 - Enhancement: Added capability to enclose portions of commands in double quotes `"` and single quotes `'`. Previously the library would error if you specified a command that had quotes.
    - Added additional testing functionality to test the passing of shell commands with double quotes `"` and single quotes `'`

## 1.0.1
 - Added pipeline integration.
 - Updated parent pom to `1.0.1`

## 1.0.0
 - Initial Release