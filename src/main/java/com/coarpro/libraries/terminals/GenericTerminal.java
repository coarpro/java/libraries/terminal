package com.coarpro.libraries.terminals;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This class can be used to run shell commands for Non Linux
 * Operating Systems. If your application will run on Linux then it is recommended
 * to build an instance of the LinuxTerminal class. If your application
 * will run on both a Non Linux OS and a Linux OS then
 * you should build an instance of this class and an instance of the LinuxTerminal
 * class. This class is generally used for Non Linux operating systems.
 */
public final class GenericTerminal extends Shell {
    private List<String> shellCommandBuilder = new ArrayList<>();

    private GenericTerminal(final GenericTerminalBuilder builder) {
        super(builder);
    }

    /**
     * It runs the shell command provided and returns its output.
     * The method calls the .setShellCommand() and .runShellCommand()
     * methods. See those methods for additional logic.
     */
    @Override
    public String runShellCommandAndGetOutput(final String shellCommand) throws TerminalException {
        // (1) Set shell command to be executed.
        setShellCommand(shellCommand);

        // (2) Run the shell command
        super.runShellCommand();

        // (3) Get its stdout
        return this.shellCommandOutput;
    }

    /**
     * The method modifies the shell command based on the
     * parameters set when building an instance of this class and
     * then sets the class instance variable.
     * If you defined a shell to run commands from then this will
     * add it to the beginning of the shell command.
     *
     * <br><br>
     *
     * For example, if your shell command to run is "echo 'hello'"
     * and you defined "C:/Windows/System32/WindowsPowerShell/v1.0/powershell.exe"
     * as the terminal to execute a shell command from then the
     * actual shell command to be executed will be.
     * <br><br>
     *
     * C:/Windows/System32/WindowsPowerShell/v1.0/powershell.exe "echo 'hello'"
     * @throws TerminalException if an error occurs setting the terminal exception.
     */
    @Override
    public void setShellCommand(final String shellCommand) throws TerminalException {
        super.setShellCommand(null);

        /*
         * Add shell command
         */
        if (this.specifiedShellFile != null) {
            prependSpecifiedShell();

        }

        addShellCommand(shellCommand);

        /*
         * This sets the command to be issued and
         * also incorporates the default logic in the
         * parent class method.
         */
        super.shellCommand = shellCommandBuilder;
    }

    /**
     * The method prepends the provided shell to the command.
     * For example, if your command is "echo hello" and you provided
     * 'C:/Windows/System32/WindowsPowerShell/v1.0/powershell.exe'
     * as the shell to execute the command, then
     * 'C:/Windows/System32/WindowsPowerShell/v1.0/powershell.exe'
     * is prepended.
     * <br>
     * <br>
     *
     * C:/Windows/System32/WindowsPowerShell/v1.0/powershell.exe -c "echo hello"
     *
     */
    private void prependSpecifiedShell() {
        String specifiedShellFileString = this.specifiedShellFile.getAbsolutePath();

        shellCommandBuilder.add(specifiedShellFileString);
        shellCommandBuilder.add("-c");
    }

    /**
     * This adds the command to be executed.
     *
     * @param commandToExecute
     *
     */
    private void addShellCommand(final String commandToExecute) {
        if (this.shellCommandBuilder.isEmpty()) {
            shellCommandBuilder.add(commandToExecute);
        } else {
            /*
            * If the arrayList is not empty then double
            * quotes are added to the beginning of the shell command
            * passed and to the end of the shell command that is passed
            * to the method.
            *
            * It is assumed that a specified shell is at the beginning
            * followed by -c
            *
            * For example, if you specified
            * "C:/Windows/System32/WindowsPowerShell/v1.0/powershell.exe"
            * as the shell to run shell commands with the command
            * 'Write-Host hello then the following command is run
            *
            * C:/Windows/System32/WindowsPowerShell/v1.0/powershell.exe -c "Write-Host hello"
            */

            // Add opening double quote
            shellCommandBuilder.add("\"");

            Collections.addAll(shellCommandBuilder, commandToExecute.split(" "));

            // Add closing double quote
            shellCommandBuilder.add("\"");
        }

    }


    public static final class GenericTerminalBuilder extends TerminalBuilder {

        private final Integer timeoutExitCode = 124;

        /**
         * This functionality has not been tested on generic/closed Operating
         * Systems such as Microsoft Windows.
         * @param userAccountToExecuteCommands
         */
        @Override
        public GenericTerminalBuilder withUserAccountToExecuteCommands(
            final String userAccountToExecuteCommands) {

            throw new UnsupportedOperationException("Running commands as a different user is not supported on this platform with this utility.");
        }

        @Override
        public Terminal build() throws TerminalException {

            return new GenericTerminal(this);
        }
    }
}
