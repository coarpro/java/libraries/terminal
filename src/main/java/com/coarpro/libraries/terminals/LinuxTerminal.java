package com.coarpro.libraries.terminals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * This class can be used to create a terminal in
 * an application that can run Shell commands against
 * a Unix environment.
 */
public final class LinuxTerminal extends Shell {
    private List<String> shellCommandsList = new ArrayList<>();

    private LinuxTerminal(final LinuxTerminalBuilder builder) {
        super(builder);
    }

    /**
     * Runs a shell command and returns its output.
     * <br><br>
     *
     * This method calls the parent classes .runShellCommand()
     * method so any additional logic set on that method will
     * also apply when calling this method.
     * See the .run() method for more details.
     *
     *
     * @param shellCommand
     * @return String
     * @throws TerminalException
     */
    @Override
    public String runShellCommandAndGetOutput(final String shellCommand) throws TerminalException {
        // (1) Set shell command to be executed.
        this.setShellCommand(shellCommand);

        // (2) Run the command.
        super.runShellCommand();

        // (3) Return the stdout
        return super.shellCommandOutput;
    }

    /**
     * The method modifies the shell command based on the
     * parameters set when building an instance of this class and
     * then sets the class instance variable.
     * If you defined a shell to run commands from then this will
     * add it to the beginning of the shell command.
     *
     * <br><br>
     *
     * For example, if your shell command to run is "echo 'hello'"
     * and you defined "/usr/bin/sh"
     * as the terminal to execute a shell command from and
     * also specified user testuser to run commands as then the actual
     * command issued is
     * <br><br>
     *
     * /usr/bin/sh su - testuser -c "echo 'hello'"
     *
     * @param commandToExecute
     * @throws TerminalException if an index out of bounds error occurs.
     */
    @Override
    public void setShellCommand(final String commandToExecute) throws TerminalException {
        // Call the parent class super method to clear values
        super.setShellCommand(null);

        /*
         * ADd the command
         */
        addShellCommand(commandToExecute);

        /*
         * Next add switch user commands
         * if applicable.
         */
        addSwitchUserCommandAndShellCommand();

        /*
         * Prepend the specified shell if
         * applicable.
         */
        prependSpecifiedShell();

        super.shellCommand = shellCommandsList;
    }

    /**
     * The method prepends the provided shell to the command.
     * For example, if your command is "echo hello" and you provided
     * '/usr/bin/bash' as the shell to execute the command, then
     * '/usr/bin/bash -c ' is prepended.
     * <br>
     * <br>
     *
     * /usr/bin/bash -c "echo hello"
     *
     */
    private void prependSpecifiedShell() {
        if (this.specifiedShellFile == null) {
            return;
        }

        String specifiedShellFileString = this.specifiedShellFile.getAbsolutePath();

        /*
         * Add to the beginning of the array.
         */
        shellCommandsList.add(0, "-c");
        shellCommandsList.add(0, specifiedShellFileString);
    }

    /**
     * The prepends the "sudo su -c " to the command to be executed.
     * For example if you specified user "foo" as the user to run
     * commands as then "foo" is added before the command.
     * <br><br>
     *
     * sudo "su - foo -c 'echo hello'"
     *
     * @throws TerminalException
     */
    private void addSwitchUserCommandAndShellCommand() throws TerminalException {

        if (this.userAccountToExecuteCommands == null || this.userAccountToExecuteCommands.isBlank()) {
            return;
        }

        try {

            /*
            * Add a single quote to the last element and the first element
            */
            String firstString = shellCommandsList.get(0);
            String quotedFirstString = String.format("'%s", firstString);
            shellCommandsList.remove(0);

            /*
            * Add to the quote to the beginning of the first string.
            * The quote is added directly to the string to prevent
            * a quote being mistaken as an actual command to run.
            */
            shellCommandsList.add(0, quotedFirstString);

            /*
            * Get index of last element in the array list
            */
            int lastElement = shellCommandsList.size() - 1;

            String lastString = shellCommandsList.get(lastElement);
            String quotedLastString = String.format("%s'", lastString);

            shellCommandsList.remove(lastElement);

            /*
            * Add the quoted string. The quoted string is
            * appended to the string instead of being
            * in a separate element to prevent mistakenly
            * running a quote as a standalone command.
            *
            */
            shellCommandsList.add(quotedLastString);

            /*
            * Now add the sudo su commands in reverse order
            * so they appear correct in the array.
            */
            shellCommandsList.add(0, "-c");
            shellCommandsList.add(0, userAccountToExecuteCommands);
            shellCommandsList.add(0, "-");
            /*
            * The escaped " to prepend the double quote to su.
            * "su ...
            */
            shellCommandsList.add(0, "su");
            shellCommandsList.add(0, "sudo");

            /*
            * Compile the final string by joining the array contents
            * clearing the array and then readding for proper formatting.
            */

            String finalString = String.join(" ", shellCommandsList);
            shellCommandsList.clear();
            shellCommandsList.add(finalString);

        } catch (IndexOutOfBoundsException e) {
            throw new TerminalException(
                'F',
                logger,
                 String.format(
                    "%s%n%nAn unexpected out of bounds error was thrown while preparing elevated commad '%s' to be run",
                    e.getMessage(), String.join(" ", shellCommandsList)
                 )
            );
        }

    }

    /**
     * This adds the command to be executed.
     *
     * @param commandToExecute - Pass the command to execute.
     *
     */
    private void addShellCommand(final String commandToExecute) {
        /*
         * If no shell was specified to run shell commands in the builder class
         * then parse paramters so that the default ProcessBuilder will process them correctly
         * ProcessBuilder takes args as an array and each element is
         * equal to a different command to run. Any command with spaces
         * will execute incorrectly unless its done this way.
         */
        if (super.specifiedShellFile == null) {
            Arrays.asList(commandToExecute.split(" ")).forEach(
                shellCommandsList::add
            );
        } else {
            /*
             * WARNING: Do not prepend and append double quotes ""
             * to the list as may be done in other terminal classes.
             * This breaks running shell commands on Linux due to the
             * way ProcessBuilder interprets commands with spacings.
             *
             * Instead, just add the command passed directly to the
             * list as is done here. The quotes should already be placed
             * in the proper portion of the command passed.
             */
            Collections.addAll(shellCommandsList, commandToExecute);
        }
    }


    /**
     * It is used to build a terminal instance.
     */
    public static final class LinuxTerminalBuilder extends TerminalBuilder {

        private final Integer timeoutExitCode = 124;

        /**
         * Specify a user to execute commands as.
         * @param userAccountToExecuteCommands
         */
        @Override
        public TerminalBuildable withUserAccountToExecuteCommands(final String userAccountToExecuteCommands) {
            this.userAccountToExecuteCommands = userAccountToExecuteCommands;

            return this;
        }

        /**
         * Builds an instance of the Linux Terminal class
         * and returns the terminal interface.
         * <br><br>
         *
         * NOTE: An instance of TerminalFactory is automatically created
         * if one was was not provided using the ".withTerminalFactory()"
         * method.
         */
        @Override
        public Terminal build() throws TerminalException {
            Terminal terminal = new LinuxTerminal(this);

            String currentUser = System.getProperty("user.name");

            if (this.userAccountToExecuteCommands != null
                && !this.userAccountToExecuteCommands.isBlank()
                && !Objects.equals(this.userAccountToExecuteCommands, currentUser)) {

                try {
                    /*
                    * Switch to the user and just list its default directory.
                    * Use sudo to prevent a password from being stored in
                    * the history.
                    */
                    terminal.setShellCommand("ls ~");

                    terminal.run();

                    if (Boolean.TRUE.equals(terminal.getHasShellCommandFailed())) {
                        throw new TerminalException('E', logger,
                            String.format("Current user '%s' is not allowed to execute commands as '%s'.%n%n%s",
                            currentUser, userAccountToExecuteCommands, terminal.getShellCommandOutput())
                        );
                    }
                } catch (TerminalException e) {
                    throw new TerminalException('E', logger,
                        String.format("Error received when performing a user switch test. It is as follows:%n%n%s",
                            e.getMessage()
                        )
                    );
                }
            }

            return new LinuxTerminal(this);
        }
    }
}
