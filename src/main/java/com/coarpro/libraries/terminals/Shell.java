package com.coarpro.libraries.terminals;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import com.coarpro.libraries.logging.ICoarproLogger;

/**
 * Coarpro Shell is an abstract class that allows for extension
 * of subclasses to issue shell commands. It contains
 * default logic and behavior with required variables.
 */
public abstract class Shell implements Terminal {
    /*
     * Variables that are static that cannot
     * be changed after being set when the
     * class instance has been created.
     */
    protected final ICoarproLogger logger;
    protected final Integer defaultMinutesToTimeOut;
    protected final String userAccountToExecuteCommands;
    protected final File specifiedShellFile;
    protected final List<Integer> ignorableExitCodes;
    protected final Integer timeoutExitCode;
    protected final Integer nonErrorExitCode = 0;
    protected final TerminalFactory terminalFactory;

    /**
     * These are variables that can
     * be dynamically set at runtime.
     */
    protected Integer oneTimeMinutesToTimeOut;
    protected List<String> shellCommand = new ArrayList<>();
    protected String shellCommandOutput;
    protected String stdout;
    protected String stderr;
    protected Boolean hasShellCommandFailed;
    protected Boolean hasShellCommandTimedOut;
    protected Integer exitCode;


    /*
     * This list should be set BEFORE you run a command.
     */
    protected List<Integer> oneTimeIgnorableExitCodes = new ArrayList<>();

    protected Shell(final TerminalBuilder builder) {
        this.logger = builder.logger;
        this.defaultMinutesToTimeOut = builder.defaultMinutesToTimeOut;
        this.ignorableExitCodes = builder.ignorableExitCodes;
        this.timeoutExitCode = builder.timeoutExitCode;
        this.userAccountToExecuteCommands = builder.userAccountToExecuteCommands;
        this.specifiedShellFile = builder.specifiedShellFile;
        //this.terminalFactory = builder.terminalFactory;

        /*
         * If the builder did not create an instance
         * of Terminal factory, then create it here.
         */
        if (builder.terminalFactory == null) {
            this.terminalFactory = new TerminalFactory();
        } else {
            this.terminalFactory = builder.terminalFactory;
        }
    }

    /**
     * Returns both the stdout and stderr from the shell
     * command issued.
     * @return String
     */
    public String getShellCommandOutput() {
        return shellCommandOutput;
    }

    /**
     * Returns the stdout from the command issued.
     * @return String
     */
    public String getStdout() {
        return returnString(this.stdout);
    }

    /**
     * Returns the stderr from the command issued.
     * @return String
     */
    public String getStderr() {
        return returnString(this.stderr);
    }

    /**
     * Returns true if the shell command has timed out.
     * Returns false otherwise.
     * @return Boolean
     */
    public Boolean getHasShellCommandTimedOut() {
        if (this.hasShellCommandTimedOut == null) {
            return false;
        }

        return hasShellCommandTimedOut;
    }

    /**
     * Returns the executed shell commands exit code.
     * @return String
     */
    public Integer getExitCode() {
        if (this.exitCode == null) {
            return this.nonErrorExitCode;
        }

        return exitCode;
    }

    /**
     * Returns true if the given command has failed
     * returns false if it has not failed. Returns null
     * if no command was issued.
     *
     * @return Boolean
     */
    @Override
    public Boolean getHasShellCommandFailed() {
        if (this.hasShellCommandFailed == null) {
            return false;
        }

        return hasShellCommandFailed;
    }

    /**
     * Returns the logger.
     *
     * @return ICoarproLogger
     */
    @Override
    public ICoarproLogger getLogger() {
        return logger;
    }

    /**
     * Returns the default minutes allowed
     * before a command times out.
     *
     * @return Integer
     */
    @Override
    public Integer getDefaultMinutesToTimeOut() {
        return defaultMinutesToTimeOut;
    }

    /**
     * Returns a list of error codes that are to be
     * ignored for all shell commands to be executed
     * with this instance.
     *
     * @return List
     */
    @Override
    public List<Integer> getIgnorableExitCodes() {
        return Collections.unmodifiableList(ignorableExitCodes);
    }

    /**
     * Returns a list of error codes that are to be
     * ignored ONCE for an UPCOMING shell command
     * that will be executed.
     *
     * @return List
     */
    @Override
    public List<Integer> getOneTimeIgnorableExitCodes() {
        return Collections.unmodifiableList(oneTimeIgnorableExitCodes);
    }

    /**
     * Provide a list of ignorable exit codes that should
     * be set for an UPCOMING command that will be issued.
     * If any of the error codes are encountered then they
     * will be ignored.
     *
     * @param oneTimeIgnorableExitCodes
     */
    @Override
    public void setOneTimeIgnorableExitCodes(final Integer... oneTimeIgnorableExitCodes) {
        this.oneTimeIgnorableExitCodes = new ArrayList<>(Collections.unmodifiableList(Arrays.asList(oneTimeIgnorableExitCodes)));

        logMessage('D', String.format("One time ignorable exit code list set to %s", this.oneTimeIgnorableExitCodes));
    }

    /**
     * Returns the time out for an upcoming command. If you
     * specified a one time timeout for an upcoming command it
     * will return that value. Otherwise it will return the
     * default timeout set for all commands.
     *
     * @return Boolean
     */
    @Override
    public Integer getOneTimeMinutesToTimeOut() {
        final Integer zero = 0;

        if (this.oneTimeMinutesToTimeOut == null || Objects.equals(this.oneTimeMinutesToTimeOut, zero)) {
            return this.defaultMinutesToTimeOut;
        } else {
            return this.oneTimeMinutesToTimeOut;
        }
    }

    /**
     * Provide an integer value to temporarily change the
     * timeout time for an upcoming command. Use this if you
     * anticipate a command will take longer than the default
     * timeout.
     *
     * @param minutesToTimeOut
     */
    @Override
    public void setOneTimeMinutesToTimeOut(final Integer minutesToTimeOut) {
        this.oneTimeMinutesToTimeOut = minutesToTimeOut;
    }

    /**
     * Returns the user account that commands are being issued
     * if one was specified when the instance was built. If you
     * didnt specify a user then it will return an empty string.
     *
     * @return String
     */
    @Override
    public String getUserAccountToExecuteCommands() {
        return returnString(this.userAccountToExecuteCommands);
    }

    /**
     * It sets the shell command to be executed. When this method is called
     * it will reset other instance variables back to their default values.
     *
     * <br><br>
     *
     * NOTE: The logic in this method contains default behavior. If you need
     * additional functionality then override this method in its own class.
     * @param shellCommandToRun
     * @throws TerminalException if the shell command fails to be executed. Check
     * the underlying classes to determine the exact cause.
     */
    @Override
    public void setShellCommand(final String shellCommandToRun) throws TerminalException {
        this.shellCommand.clear();
        this.shellCommandOutput = null;
        this.stdout = null;
        this.stderr = null;
        this.hasShellCommandFailed = false;
        this.hasShellCommandTimedOut = false;
        this.exitCode = this.nonErrorExitCode;

        if (shellCommandToRun == null || shellCommandToRun.isBlank()) {
            logMessage('D',
            "A call to run a shell command was given but the command to execute was not set, or is empty."
            );
        } else {
            this.shellCommand.add(shellCommandToRun);
        }
    }

    /**
     * It runs a shell command and throws an exception if the
     * command fails. This method calls the .run() method so
     * any logic or behavior included in that method will also
     * occur here. See the .run() method for additional details.
     */
    @Override
    public void runShellCommand() throws TerminalException {
        logMessage('D',
            String.format("Shell command to be issued is %s", this.shellCommand)
        );

        /*
         * Call the .run() method
         */
        run();

        if (Boolean.TRUE.equals(this.hasShellCommandFailed)) {
            throw new TerminalException('E', logger,
                String.format("A shell command failed to complete successfully.%n%n%s",
                    this.shellCommandOutput)
            );
        }
    }

    /**
     * The method runs the command passed and will attempt to get its stdout and stderr.
     *
     * if the command runs longer than the allocated time then this method will killl it.
     * If you have set any one time exit codes that should be ignored then this method
     * will clear them AFTER the command has been executed. This is to prevent
     * the next command to be executed from inheriting exit codes that should
     * not be ignored. <br><br>
     *
     * If you passed a list of exit codes that should always be ignored when building
     * this instance then any command run under this instance will not raise an error
     * if any of those error codes are encountered. <br><br>
     *
     * The command will also reset the one time timeout back to the default if you
     * specified a one time limit for a particular command. <br><br>
     *
     * This method also overrides the Runnable interfaces .run() method to allow
     * multi threading support to prevent blocking thus allowing you to
     * proceed to perform additional tasks while a command runs.
     *
     */
    @Override
    public void run() {
        if (this.shellCommand == null || this.shellCommand.isEmpty()) {
            logMessage('T',
                "The run method was called but the variable that stores the shell command to execute is empty or null.");
            return;
        }


        try {

            logMessage('D', String.format("The following shell command is being executed: %n%n%s",
                String.join(",", shellCommand))
            );

            ProcessBuilder processBuilder = terminalFactory.createProcessBuilder();

            // (1) Execute the command
            processBuilder.command(this.shellCommand);
            Process process = processBuilder.start();

            //Process process = Runtime.getRuntime().exec(this.shellCommand);

            // (2) Terminate the command if the time elapses passed the time limit.
            killIfTimeLimitElapses(process);

            // (3) Get the commands exit code.
            setExitCode(process);

            // (4) Set the stdout and stderr.
            setStdOutAndStdErr(process);

        } catch (IOException e) {
            this.hasShellCommandFailed = true;

            logMessage('E',
                String.format("The shell command %s encountered an IO exception with the following error message:%n%n%s",
                    shellCommand, e.getMessage())
            );

        } catch (InterruptedException e) {
            this.hasShellCommandFailed = true;

            logMessage('E',
                String.format("The shell command %s was interrupted before it could finish executing and generated message:%n%n%s",
                    shellCommand, e.getMessage())
            );
        } catch (IllegalThreadStateException e) {
            this.hasShellCommandFailed = true;

            logMessage('S',
                String.format("An illegal thread error has occured running shell command %s%n%n%s", shellCommand, e.getMessage())
            );
        }

        // (5) Reset one time timeout limit back to default
        this.oneTimeMinutesToTimeOut = this.defaultMinutesToTimeOut;

        /*
        * Clear the onetime exit codes list after each
        * under the assumption that another command be
        * executed.
        */
        oneTimeIgnorableExitCodes.clear();
    }

    /**
     * It kills the process only if the time limit
     * for the command elapses.
     * @param process
     * @throws InterruptedException
     */
    private void killIfTimeLimitElapses(final Process process) throws InterruptedException {
        Boolean isCommandFinished = process.waitFor(getOneTimeMinutesToTimeOut(), TimeUnit.MINUTES);

        /*
        * It will return true if the command
        * finished within its time limit otherwise
        * it will return false.
        */
        if (Boolean.TRUE.equals(isCommandFinished)) {
            logMessage('D', String.format(
                "Shell command %s has completed within the time limit of %s minutes",
                shellCommand, getOneTimeMinutesToTimeOut())
            );

        } else {
            logMessage('D',
                String.format("Shell command %s is being killed because its time limit of %s has been reached.",
                    shellCommand, getOneTimeMinutesToTimeOut()
                )
            );

            // Force kill the process.
            this.hasShellCommandTimedOut = true;
            process.destroyForcibly();
        }
    }

    /**
     * Its retrieves and sets the exit code returned from the command executed.
     * @param process
     */
    private void setExitCode(final Process process) {
        logMessage('T', String.format("Obtaining exit code for shell command '%s'", shellCommand));

        /*
        * The conditional check exists because on Linux
        * an IllegalThreadStateException is called
        * when the process if focibly destroyed.
        * OpenJDK 17 as of 9/26/2023
        */
        if (Boolean.TRUE.equals(this.hasShellCommandTimedOut)) {
            logMessage('E', String.format("The shell command '%s' did not complete within the specified time limit of %s minutes",
                shellCommand, getOneTimeMinutesToTimeOut())
            );

            this.hasShellCommandFailed = true;
            this.exitCode = this.timeoutExitCode;
        } else {
            // .exitValue() only works if the process was not forcibly destroyed.
            this.exitCode = process.exitValue();
        }
    }

    /**
     * The method sets the class instance variables for
     * stdout and stderr and sets the returned value to the
     * following: <br><br>
     *
     * If no error occurs then only the stdout is returned.<br><br>
     *
     * If an error occurs then headers [STDOUT] and [STDERR] are added.
     * The returned format is as follows:<br><br>
     * [STDOUT]<br><br>
     * Some stdout
     * <br><br>
     * [STDERR]<br><br>
     * Some stderr
     *
     * @param process
     *
    */
    private void setStdOutAndStdErr(final Process process) throws IOException {
        StringBuilder shellOutputBuilder = new StringBuilder();

        Boolean includeStdOutStdErrHeaders = false;

        if (this.ignorableExitCodes.contains(this.exitCode) || this.oneTimeIgnorableExitCodes.contains(this.exitCode)) {
            logMessage('D',
                String.format("Shell command %s completed with ignorable exit code %s",
                    this.shellCommand, this.exitCode
                )
            );

            includeStdOutStdErrHeaders = true;
            this.stderr = getShellCommandStdErr(process);

        } else if (!Objects.equals(this.exitCode, nonErrorExitCode)) {
            includeStdOutStdErrHeaders = true;
            this.hasShellCommandFailed = true;
            this.stderr = getShellCommandStdErr(process);

        } else {
            logMessage('D',
                String.format("Shell command '%s' executed successfully with exit code %s", shellCommand, exitCode)
            );
        }

        this.stdout = getShellCommandStdOut(process);

        if (Boolean.TRUE.equals(includeStdOutStdErrHeaders)) {
            shellOutputBuilder.append(String.format("[STDOUT]%n"));
            shellOutputBuilder.append(this.stdout);
            shellOutputBuilder.append(String.format("%n"));
            shellOutputBuilder.append(String.format("[STDERR]%n"));
            shellOutputBuilder.append(this.stderr);
        } else {
            shellOutputBuilder.append(this.stdout);
        }

        this.shellCommandOutput = shellOutputBuilder.toString().strip();
    }

    /**
     * It reads the stdout form the command executed.
     * @param process
     * @return String
     * @throws IOException
     */
    private String getShellCommandStdOut(final Process process) throws IOException {
        BufferedReader reader
            = new BufferedReader(new InputStreamReader(
                process.getInputStream(), StandardCharsets.UTF_8));

        String line;

        StringBuilder stdoutBuilder = new StringBuilder();

        /*
        * Add to shellOutput
        */
        while ((line = reader.readLine()) != null) {
            stdoutBuilder.append(line + "\n");
        }

        reader.close();
        return stdoutBuilder.toString().strip();
    }

    /**
     * The method first gets the stdout by calling getShellCommandStdout method.
     * It will then get the stderr so this method returns
     * both the stdout and the stderr. The returned format is as follows:<br><br>
     * [STDOUT]<br><br>
     * Some stdout
     * <br><br>
     * [STDERR]<br><br>
     * Some stderr
     * @param process
     * @return String
     * @throws IOException
     */
    private String getShellCommandStdErr(final Process process) throws IOException {
        BufferedReader reader
            = new BufferedReader(new InputStreamReader(
                process.getErrorStream(), StandardCharsets.UTF_8));


        StringBuilder stdErr = new StringBuilder();

        // Get stderr output
        String line;

        while ((line = reader.readLine()) != null) {
            stdErr.append(line + "\n");
        }

        reader.close();

        return stdErr.toString().strip();
    }

    /**
     * Checks if the value is null and if so
     * returns an empty string as opposed to
     * return null to reduce NullPointerException Exceptions.
     * @param value
     * @return String
     */
    private String returnString(final String value) {
        if (value == null) {
            return "";
        }

        return value;
    }

    /**
     * Logs a message. Use the following as a guide for the
     * logMessageType.<br><br>
     *
     * "I" = info <br><br>
     * "D" = debug <br><br>
     * "T" = trace <br><br>
     * "e" = error <br><br>
     * "f" = fatal <br><br>
     * "s" = severe <br><br>
     * @param logMessageType
     * @param message
     */
    protected void logMessage(final char logMessageType, final String message) {
        if (logger == null) {
            return;
        }

        switch (String.valueOf(logMessageType).toLowerCase()) {
            case "i":
                logger.logInfoMessage(message);
                break;
            case "d":
                logger.logDebugMessage(message);
                break;
            case "t":
                logger.logTraceMessage(message);
                break;
            case "e":
                logger.logErrorMessage(message);
                break;
            case "f":
                logger.logFatalMessage(message);
                break;
            case "s":
                logger.logSevereMessage(message);
                break;
            default:
                break;
        }

    }

    public abstract static class TerminalBuilder implements TerminalBuildable {
        protected Integer defaultMinutesToTimeOut = 15;
        protected List<Integer> ignorableExitCodes = new ArrayList<>();
        protected Integer timeoutExitCode = 124;
        protected String userAccountToExecuteCommands;
        protected ICoarproLogger logger;
        protected File specifiedShellFile;
        protected TerminalFactory terminalFactory;

        /*
         * -------------------------------------------------------
         * Below methods are default logic and behavior.
         * If different functionality is required in a class that
         * extends this class then override the method in its
         * respective class file and add required logic.
         *
         * The .build() method is implicitly excluded to force
         * creating a .build() method in each class that extends
         * this class.
         * --------------------------------------------------------
         */

        /**
         * Default logic and value.
         *
         * Specify the default timeout period
         * for all shell commands that will be
         * issued with this instance.
         *
         * @param minutesToTimeOut
         * @return TerminalBuildable
         */
        public TerminalBuildable withDefaultMinutesToTimeOut(final Integer minutesToTimeOut) {
            this.defaultMinutesToTimeOut = minutesToTimeOut;

            return this;
        }

        /**
         * Default logic and value.
         *
         * @param userAccountToExecuteCommands
         * @return TerminalBuildable
         */
        public TerminalBuildable withUserAccountToExecuteCommands(final String userAccountToExecuteCommands) {
            throw new UnsupportedOperationException("Unimplemented method 'withUserAccountToExecuteCommands'");
        }

        /**
         * Default logic and value.
         *
         * @param logger
         * @return TerminalBuildable
         */
        public TerminalBuildable withLogger(final ICoarproLogger logger) {
            this.logger = logger;

            return this;
        }

        /**
         * Default logic and value.
         *
         * @param ignorableExitCodes
         * @return TerminalBuildable
         */
        public TerminalBuildable withIgnorableExitCodes(final Integer... ignorableExitCodes) {
            this.ignorableExitCodes = Collections.unmodifiableList(Arrays.asList(ignorableExitCodes));

            return this;
        }

        /**
         * Default logic and value.
         * It just verifies that the specified shell or program
         * to run commands exists, is a file and is executable.
         * If it's not then it throws an exception.
         *
         * @param fullyQualifiedFileName
         * @return TerminalBuildable
         */
        public TerminalBuildable withSpecifiedShell(final File fullyQualifiedFileName) throws TerminalException {
            if (fullyQualifiedFileName.exists()
                && fullyQualifiedFileName.isFile()
                && fullyQualifiedFileName.canExecute()) {
                    this.specifiedShellFile = fullyQualifiedFileName;
                } else {
                    throw new TerminalException('E', logger,
                        String.format("The provided shell file '%s' does not exist, or is inaccesible", fullyQualifiedFileName.getAbsolutePath()));
                }

            return this;
        }

        /**
         * Pass an instance of TerminalFactory. If an instance is not
         * passed then the constructor will automatically create one.
         *
         * @param terminalFactory
         * @return TerminalBuildable
         */
        @Override
        public TerminalBuildable withTerminalFactory(final TerminalFactory terminalFactory) {
            this.terminalFactory = terminalFactory;

            return this;
        }
    }
}
