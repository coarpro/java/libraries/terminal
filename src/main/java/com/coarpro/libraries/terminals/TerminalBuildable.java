package com.coarpro.libraries.terminals;

import java.io.File;

import com.coarpro.libraries.logging.ICoarproLogger;

/**
 * The terminable interface is used for creating
 * a new Terminal class builder for an Operating System Terminal
 * class  to ensure that it has the same or similar functionality as
 * other Terminal classes.
 */
public interface TerminalBuildable {

        /**
         * When building a class specify the default timeout period
         * allowed for all commands. This will ensure that any commands
         * run through this instance will automatically timeout if the
         * command runs longer than the time allocated.
         * @param minutesToTimeOut
         * @return TerminalBuildable
         */
        TerminalBuildable withDefaultMinutesToTimeOut(Integer minutesToTimeOut);

        /**
         * Only call this if you plan to run commands as a different user than the current user, otherwise
         * do not call this method. All commands will be run as this user with this instance.
         *
         * @param userAccountToExecuteCommands
         * @return TerminalBuildable
         */
        TerminalBuildable withUserAccountToExecuteCommands(String userAccountToExecuteCommands);

        /**
         * Specify the logger instance.
         * @param logger
         * @return TerminalBuildable
         */
        TerminalBuildable withLogger(ICoarproLogger logger);

        /**
         * Specify a commary separated value of interger exit codes that should be
         * ignored from raising an exception. You do not need to include exit code 0.
         * @param ignorableExitCodes
         * @return TerminalBuildable
         */
        TerminalBuildable withIgnorableExitCodes(Integer... ignorableExitCodes);

        /**
         * If you prefer to use a different Shell instead of the
         * default shell to run commands with the instance you are building
         * then you can specify its full qualified file path.
         * <br>
         * <br>
         *
         * For example: In Linux you may want use to the Korn Shell
         * "/usr/bin/ksh" instead of the default which is usually "/usr/bin/bash"
         * or "/usr/bin/sh".
         *
         * <br>
         * <br>
         * In Windows, you may want to use Powershell instead of "cmd"
         * so you may specify "C:/Windows/System32/WindowsPowerShell/v1.0/powershell.exe".
         *
         * @param fullyQualifiedFileName - Specify the fully qualified file name.
         * The method will verify the file exists, is a file, and is executable.
         * It will raise a CoarproTerminalException if it doesnt.
         *
         * @return TerminalBuildable
         */
        TerminalBuildable withSpecifiedShell(File fullyQualifiedFileName) throws TerminalException;

        /**
         * The TerminalFactory class can optionally be specified. If you
         * dont provide a TerminalFactory instance then one should be automatically
         * created when an instance is built. Providing this can make unit testing
         * easier if the need arises to mock and stub some behavior.
         *
         * @param terminalFactory - Provide an instance of TerminalFactory.
         * @return TerminalBuildable
         */
        TerminalBuildable withTerminalFactory(TerminalFactory terminalFactory);

        /**
         * When this method is called it will create an instance of a terminal class
         * and return IOsTerminal interface.
         *
         * <br><br>
         *
         * NOTE: If you specified a different user account to execute commands
         * then this method will attempt to switch to that user first and then run a test command
         * to determine if the user has the capability to switch to that user. If it fails to switch
         * to that user then it will raise an exception. If you did not specify a user
         * to run commands as then it will use the current user
         * @return Terminal
         * @throws TerminalException
         */
        Terminal build() throws TerminalException;
}
