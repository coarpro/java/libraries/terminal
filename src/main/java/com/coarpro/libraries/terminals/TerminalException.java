package com.coarpro.libraries.terminals;

import com.coarpro.libraries.logging.ICoarproLogger;

public class TerminalException extends Exception {
    private TerminalException(final String errorMessage) {
        super(errorMessage);
    }

    /**
     * Specify the error type, instance of the Coarpro Logger, and the
     * message to be written to the log.
     * @param logErrorMessage - specify the log error message.
     * @param errorType Specify either "F" for Fatal. "E" for Error, or "S" for Severe.
     * @param logger
     * @throws TerminalException
     */
    public TerminalException(final char errorType, final ICoarproLogger logger, final String logErrorMessage)
        throws TerminalException {

        if (logger != null) {
            switch (errorType) {
                case 'S':
                    logger.logSevereMessage(logErrorMessage);
                    break;
                case 'F':
                    logger.logFatalMessage(logErrorMessage);
                    break;
                case 'E':
                    logger.logErrorMessage(logErrorMessage);
                    break;
                default:
                    break;
            }
        }

        throw new TerminalException(logErrorMessage);
    }

}
