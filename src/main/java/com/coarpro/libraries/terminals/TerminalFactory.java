package com.coarpro.libraries.terminals;

/**
 * The TerminalFactory class is used to create objects
 * needed for proper functioning of the Terminal classes
 * that work with their parent abstract class 'Shell'.
 */
public class TerminalFactory {
    /**
     * It creates an instance of the ProcessBuilder
     * class and returns it.
     * @return an instance of ProcessBuilder
     */
    public ProcessBuilder createProcessBuilder() {
        return new ProcessBuilder();
    }
}
