package com.coarpro.libraries.terminals;

import java.util.List;

import com.coarpro.libraries.logging.ICoarproLogger;

public interface Terminal extends Runnable {

    /**
     * It stores the command that is to be run.
     * When you call the .run() method it will
     * execute the command provided.
     * @param shellCommand
     * @throws TerminalException if an error occurs setting the shell command.
     */
    void setShellCommand(String shellCommand) throws TerminalException;

    /**
     * It runs a shell command and returns its output.
     * It throws an error if an error occurs running
     * the command.
     * @param shellCommand
     * @return String.
     */
    String runShellCommandAndGetOutput(String shellCommand) throws TerminalException;

    /**
     * Returns the Coarpro Logger for this instance.
     *
     * @return ICoarproLogger
     */
    ICoarproLogger getLogger();

    /**
     * Returns the default time out in minutes to wait before killing
     * a command. This is set when building an instance of the class.
     *
     * @return Integer
     */
    Integer getDefaultMinutesToTimeOut();

    /**
     * Returns a list of permanent exit codes that are to be ignored
     * when any command is run. This is set when building
     * an instance of the class.
     *
     * @return List
     */
    List<Integer> getIgnorableExitCodes();

    /**
     * Return a list of one time ignroable exit codes that will
     * be ignored after the command is run.
     *
     * @return List
     */
    List<Integer> getOneTimeIgnorableExitCodes();


    /**
     * Specify a comma separated value or just an Integer[] of exit codes that should be ignored
     * ONE time BEFORE a command is run. If any error codes are hit when a command is executed
     * then no exception will be raised.
     *
     * This will instruct the method "runShellCommand" to clear the list of ignorable exit
     * codes if one of the exit codes occur.
     *
     * @param oneTimeIgnorableExitCodes
     */
    void setOneTimeIgnorableExitCodes(Integer... oneTimeIgnorableExitCodes);

    /**
     * If a value was not provided for a command to timeout then it will
     * return the default timeout in minutes that was set or specified when building
     * the instance using the builder class. Otherwise it will return the value set provided
     * when you called the method "setOneTimeMinutesToTimeOut".
     * <br>
     * <br>
     *
     * NOTE: Current logic at the time this code was written (9/26/2023) dictates
     * that AFTER a command is executed that the value be reset to the default timeout
     * in minutes so it will return the defaultTimeOutInMinutes AFTER a command is executed.
     *
     * @return Integer
     */
    Integer getOneTimeMinutesToTimeOut();

    /**
     * If you call this method and provide an integer value it will adjust
     * the timeout ONLY for the upcoming command to be issued. After the command
     * has been run the timeout will the return to the standard timeout set when
     * building the instance.
     *
     * @param minutesToTimeOut
     */
    void setOneTimeMinutesToTimeOut(Integer minutesToTimeOut);

    /**
     * Return the user that commands are being executed as.
     *
     * @return String
     */
    String getUserAccountToExecuteCommands();

    /**
     * Returns both the stdout and stderr from the shell
     * command issued.
     *
     * @return String
     */
    String getShellCommandOutput();

    /**
     * Returns the stdout from the command issued.
     *
     * @return String
     */
    String getStdout();

    /**
     * Returns the stderr from the command issued.
     *
     * @return String
     */
    String getStderr();

    /**
     * Returns true if the shell command has timed out.
     * Returns false otherwise.
     *
     * @return String
     */
    Boolean getHasShellCommandTimedOut();;

    /**
     * Returns true if the given command has failed
     * returns false if it has not failed. Returns null
     * if no command was issued.
     *
     * @return Boolean
     */
    Boolean getHasShellCommandFailed();

    /**
     * Returns the executed shell commands exit code.
     *
     * @return Integer
     */
    Integer getExitCode();

    /**
     * It runs a shell command.
     */
    void runShellCommand() throws TerminalException;
}
