package com.coarpro.libraries.terminals;


import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.io.File;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)

/*
 * This is solely to be able to use
 * @BeforeAll to prevent a "must be static"
 * exception from being raised.
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TerminalTest
{

    Terminal terminal;
    private String os;

    @BeforeAll
    public void setupTerminal() throws TerminalException {
        /*
         * Get the proper OS for testing.
         */
        String operatingSystem = System.getProperty("os.name").toLowerCase().strip();

        if (operatingSystem.contains("windows")) {
            this.os = "windows";

            File powerShellFilePath = new File("C:/Windows/System32/WindowsPowerShell/v1.0/powershell.exe");

            this.terminal = new GenericTerminal.GenericTerminalBuilder()
                                          .withSpecifiedShell(powerShellFilePath)
                                          .withDefaultMinutesToTimeOut(5)
                                          .build();
        } else {
            this.os = "linux";
            File bashShellPath = new File("/usr/bin/bash");
            this.terminal = new LinuxTerminal.LinuxTerminalBuilder()
                                             .withDefaultMinutesToTimeOut(5)
                                             .withSpecifiedShell(bashShellPath)
                                             .build();
        }
    }

    /**
     * This test validates that the program raises an error
     * when the program fails.
     * @throws TerminalException
     */
    @Test
    void testRaisingErrorWhenShellCommandFails() throws TerminalException {
        assertThrows(TerminalException.class, () -> {terminal.runShellCommandAndGetOutput("echos hello");});
    }

    /**
     * This test validates that when a shell command runs
     * longer than its timeout limit that it is then halted and
     * an exception is raised.
     */
    @Test
    void testRaisingErrorWhenAShellCommandTimesOut() throws TerminalException {
        // Set timeout to 1 minute and issue sleep command for 3 minutes
        terminal.setOneTimeMinutesToTimeOut(1);

        assertThrows(TerminalException.class, () -> {terminal.runShellCommandAndGetOutput("sleep 180");});
    }

    /**
     * This test validates instance variables
     * are cleared and reset to their default values
     * before the next shell command
     * is executed. It intentionally issues a command
     * that will fail so that default instance variables
     * are no longer set to their default values and
     * then sets another command to execute to verify
     * those values got reset.
     * @throws TerminalException
     */
    @Test
    void testResettingClassInstanceVariables() throws TerminalException {
        String shellCommand = "echkhkos hello";
        /*
         * (1) Execute a shell command that will fail
         * to change default values.
         */
        terminal.setShellCommand(shellCommand);
        terminal.run();

        // (2) Verify values are no longer defaults.
        terminal.setShellCommand(shellCommand);
        terminal.run();
        assertNotEquals(0, terminal.getExitCode());
        assertFalse(terminal.getStderr().isBlank(), terminal.getStderr());
        assertTrue(terminal.getHasShellCommandFailed());


        // (3) Set another command to be executed
        String shellCommand2 = "echo hello2";
        terminal.setShellCommand(shellCommand2);

        // (4) Verify the values were reset before calling .run()
        assertTrue(terminal.getStdout().isBlank());
        assertTrue(terminal.getStderr().isBlank());
        assertFalse(terminal.getHasShellCommandFailed());
        assertFalse(terminal.getHasShellCommandTimedOut());
        assertEquals(0, terminal.getExitCode());
    }

    /**
     * This test verifies that after you set a one time
     * timeout limit for a command that it will reset itself back to the
     * default time out limit that was specified when creating
     * an instance.
     */
    @Test
    void testResettingOneTimeTimeoutLimit() throws TerminalException {
        // (1) Set a shell command to run
        terminal.setShellCommand("echo hello");

        // (2) Set a one time timeout limit
        terminal.setOneTimeMinutesToTimeOut(1);

        // (3) Verify the timeout limit was set
        assertEquals(1, terminal.getOneTimeMinutesToTimeOut());

        // (4) Run the command
        terminal.run();

        /*
         * (5) Verify the one time time out minutes
         * have been reset back to the default timeout
         * now that the command has finished.
         */
        assertEquals(5, terminal.getOneTimeMinutesToTimeOut());
    }

    /**
     * This tests both of the following:
     *
     * 1. That an exception is not raised when a one time ignroable
     * exit code has been hit
     *
     * and
     *
     * 2. That it clears the list of one time exit codes
     * for that command after the command has been issued.
     */
    @Test
    void testNotRaisingErrorWithIgnorableExitCode() {

        // (1) Create list of onetime ignorable exit codes
        Integer[] oneTimeIgnorableExitCodes = new Integer[]{1, 127};

        // (2)
        terminal.setOneTimeIgnorableExitCodes(oneTimeIgnorableExitCodes);

        // (3) Verify the the error code is ignored.
        assertDoesNotThrow(() -> { terminal.runShellCommandAndGetOutput("echjlos 'some command'"); } );

        // (4) Verify the one time ignorable exit code list has been cleared in the instance.
        assertTrue(terminal.getOneTimeIgnorableExitCodes().isEmpty());
    }
}
